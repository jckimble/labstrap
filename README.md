# LabStrap
[![pipeline status](https://gitlab.com/jckimble/labstrap/badges/master/pipeline.svg)](https://gitlab.com/jckimble/labstrap/commits/master)

LabStrap is a tool to download bootstrap files from gitlab snippets. Made cause boilerplate git repos can pile up with small changes between files.

---
* [Install](#install)
* [Configuration](#configuration)
* [Run](#run)
* [License](#license)

---

## Install
```sh
go get -u gitlab.com/jckimble/labstrap
```

## Configuration
Config File must exist as `~/.config/labstrap.yaml` or changed with --config
```yaml
token: "your-gitlab-token"
overwrite: false
gitlab: "https://gitlab.com/api/v4"
quiet: false
```

## Run
```sh
$labstrap
LabStrap is a tool to download bootstrap files from gitlab snippets

Usage:
  labstrap [command]

Available Commands:
  help        Help about any command
  pull        Pulls Gitlab Snippets into directory
  push        Creates Gitlab Snippet
  remove      Removes GitLab Snippets

Flags:
  -c, --config string   config file (default "~/.config/labstrap.yaml")
      --gitlab string   gitlab api (default "https://gitlab.com/api/v4")
  -h, --help            help for labstrap
  -q, --quiet           quiet mode
  -t, --token string    gitlab private token

Use "labstrap [command] --help" for more information about a command.
```

## License

Copyright 2018 James Kimble

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
