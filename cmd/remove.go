// Copyright © 2018 James Kimble
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"log"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// removeCmd represents the remove command
var removeCmd = &cobra.Command{
	Use:   "remove",
	Short: "Removes GitLab Snippets",
	PreRunE: func(cmd *cobra.Command, args []string) error {
		if len(args) == 0 {
			return fmt.Errorf("Must supply snippet titles")
		}
		return nil
	},
	RunE: func(cmd *cobra.Command, args []string) error {
		for _, arg := range args {
			s, err := findSnippet(arg)
			if err != nil {
				return err
			}
			if !viper.GetBool("quiet") {
				log.Printf("Removing Snippet: %s", arg)
			}
			if err := removeSnippet(s.Id); err != nil {
				return err
			}
		}
		return nil
	},
}

func init() {
	rootCmd.AddCommand(removeCmd)
}

func removeSnippet(id uint64) error {
	resp, err := submitRequest("DELETE", fmt.Sprintf("%s/snippets/%d", viper.GetString("gitlab"), id), nil)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	return nil
}
