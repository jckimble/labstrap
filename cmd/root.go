// Copyright © 2018 James Kimble
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"fmt"
	"log"
	"os"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var cfgFile string

// rootCmd represents the base command when called without any subcommands
var rootCmd = &cobra.Command{
	Use:   "labstrap",
	Short: "LabStrap is a tool to download bootstrap files from gitlab snippets",
}

// Execute adds all child commands to the root command and sets flags appropriately.
// This is called by main.main(). It only needs to happen once to the rootCmd.
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		fmt.Println(err)
		os.Exit(1)
	}
}

func init() {
	cobra.OnInitialize(initConfig)

	rootCmd.PersistentFlags().StringVarP(&cfgFile, "config", "c", "~/.config/labstrap.yaml", "config file")
	rootCmd.PersistentFlags().StringP("token", "t", "", "gitlab private token")
	rootCmd.PersistentFlags().String("gitlab", "https://gitlab.com/api/v4", "gitlab api")
	rootCmd.PersistentFlags().BoolP("quiet", "q", false, "quiet mode")
	viper.BindPFlag("gitlab", rootCmd.PersistentFlags().Lookup("gitlab"))
	viper.BindPFlag("token", rootCmd.PersistentFlags().Lookup("token"))
	viper.BindPFlag("quiet", rootCmd.PersistentFlags().Lookup("quiet"))
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	file, err := homedir.Expand(cfgFile)
	if err != nil {
		log.Fatal(err.Error())
	}
	cfgFile = file
	viper.AutomaticEnv() // read in environment variables that match
	if _, err := os.Stat(cfgFile); err == nil {
		viper.SetConfigFile(cfgFile)
		if err := viper.ReadInConfig(); err != nil {
			log.Fatal(err.Error())
		}
	}
	if viper.GetString("token") == "" {
		log.Fatal("Your GitLab Private Token must be set for this tool to work")
	}
}
