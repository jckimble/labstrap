// Copyright © 2018 James Kimble
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"encoding/json"
	"fmt"
	"log"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// manifestCmd represents the manifest command
var manifestCmd = &cobra.Command{
	Use:   "manifest",
	Short: "Uploads Manifest for snippet group",
	PreRunE: func(cmd *cobra.Command, args []string) error {
		if len(args) == 0 {
			return fmt.Errorf("Must Supply Atleast One Snippet Title")
		}
		return nil
	},
	RunE: func(cmd *cobra.Command, args []string) error {
		title := cmd.Flags().Lookup("name").Value.String()
		visibility := cmd.Flags().Lookup("visibility").Value.String()
		manifest := manifest{
			BaseDir: cmd.Flags().Lookup("basedir").Value.String(),
			Snippets: []struct {
				Id    uint64 `json:"id"`
				Title string `json:"title"`
			}{},
		}
		for _, arg := range args {
			s, err := findSnippet(arg)
			if err != nil {
				log.Fatal(err)
			}
			manifest.Snippets = append(manifest.Snippets, struct {
				Id    uint64 `json:"id"`
				Title string `json:"title"`
			}{
				Id:    s.Id,
				Title: s.Title,
			})
		}
		content, err := json.Marshal(manifest)
		if err != nil {
			return err
		}
		s, err := findSnippet(title)
		if err != nil {
			if !viper.GetBool("quiet") {
				log.Printf("Uploading Manifest: %s", title)
			}
			if visibility == "" {
				visibility = "private"
			}
			if err := uploadSnippet(title, visibility, "labstrap.manifest", content); err != nil {
				return err
			}
			return nil
		}
		if !viper.GetBool("overwrite") {
			return fmt.Errorf("manifest already exists and overwrite is not set %s", title)
		}
		if !viper.GetBool("quiet") {
			log.Printf("Updating Manifest: %s", title)
		}
		if visibility == "" {
			visibility = s.Visibility
		}
		if err := updateSnippet(s.Id, visibility, "labstrap.manifest", content); err != nil {
			return err
		}
		return nil
	},
}

func init() {
	pushCmd.AddCommand(manifestCmd)
	manifestCmd.Flags().StringP("basedir", "b", "", "Base Directory")
	manifestCmd.Flags().String("visibility", "private", "Change Visibility of File")
	manifestCmd.Flags().BoolP("overwrite", "o", false, "overwrite files")
	viper.BindPFlag("overwrite", manifestCmd.Flags().Lookup("overwrite"))
}
