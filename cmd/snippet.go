// Copyright © 2018 James Kimble
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"time"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// snippetCmd represents the snippet command
var snippetCmd = &cobra.Command{
	Use:   "snippet",
	Short: "Upload File as Gitlab Snippet",
	PreRunE: func(cmd *cobra.Command, args []string) error {
		if len(args) != 1 {
			return fmt.Errorf("Only one file accepted")
		}
		return nil
	},
	RunE: func(cmd *cobra.Command, args []string) error {
		title := cmd.Flags().Lookup("name").Value.String()
		visibility := cmd.Flags().Lookup("visibility").Value.String()
		filename := args[0]
		content, err := ioutil.ReadFile(filename)
		if err != nil {
			return err
		}
		s, err := findSnippet(title)
		if err != nil {
			if !viper.GetBool("quiet") {
				log.Printf("Uploading File: %s", filename)
			}
			if visibility == "" {
				visibility = "private"
			}
			if err := uploadSnippet(title, visibility, filename, content); err != nil {
				return err
			}
			return nil
		}
		if !viper.GetBool("overwrite") {
			return fmt.Errorf("gitlab snippet already exists and overwrite is not set %s", title)
		}
		if !viper.GetBool("quiet") {
			log.Printf("Updating Snippet: %s", title)
		}
		if visibility == "" {
			visibility = s.Visibility
		}
		if err := updateSnippet(s.Id, visibility, filename, content); err != nil {
			return err
		}
		return nil
	},
}

func init() {
	pushCmd.AddCommand(snippetCmd)
	snippetCmd.Flags().String("filename", "", "Change Filename of File")
	snippetCmd.Flags().String("visibility", "", "Change Visibility of File (default \"private\")")
	snippetCmd.Flags().BoolP("overwrite", "o", false, "overwrite files")
	viper.BindPFlag("overwrite", snippetCmd.Flags().Lookup("overwrite"))
}

func updateSnippet(id uint64, visibility string, filename string, content []byte) error {
	resp, err := submitRequest("PUT", fmt.Sprintf("%s/snippets/%d", viper.GetString("gitlab"), id), map[string]interface{}{
		"file_name":  filename,
		"content":    string(content),
		"visibility": visibility,
	})
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	s := snippet{}
	dec := json.NewDecoder(resp.Body)
	if err := dec.Decode(&s); err != nil {
		return err
	}
	if s.Id != id {
		return fmt.Errorf("Snippet Doesn't Match")
	}
	return nil
}

func uploadSnippet(title, visibility, filename string, content []byte) error {
	resp, err := submitRequest("POST", fmt.Sprintf("%s/snippets", viper.GetString("gitlab")), map[string]interface{}{
		"title":      title,
		"file_name":  filename,
		"content":    string(content),
		"visibility": visibility,
	})
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	s := snippet{}
	dec := json.NewDecoder(resp.Body)
	if err := dec.Decode(&s); err != nil {
		return err
	}
	if s.Title != title || s.FileName != filename {
		return fmt.Errorf("Snippet Doesn't Match")
	}
	return nil
}

func submitRequest(method string, url string, jsonvar map[string]interface{}) (*http.Response, error) {
	client := &http.Client{
		Timeout: 30 * time.Second,
	}
	var data io.ReadWriter
	if jsonvar != nil {
		data = bytes.NewBuffer([]byte(""))
		enc := json.NewEncoder(data)
		if err := enc.Encode(jsonvar); err != nil {
			return nil, err
		}
	}
	req, err := http.NewRequest(method, url, data)
	if err != nil {
		return nil, err
	}
	req.Header.Add("PRIVATE-TOKEN", viper.GetString("token"))
	if jsonvar != nil {
		req.Header.Add("Content-Type", "application/json")
	}
	resp, err := client.Do(req)
	if err != nil {
		return nil, err
	}
	if resp.StatusCode != 204 && resp.StatusCode != 201 && resp.StatusCode != 200 {
		defer resp.Body.Close()
		m := struct {
			Error   string `json:"error"`
			Message string `json:"message"`
		}{}
		dec := json.NewDecoder(resp.Body)
		if err := dec.Decode(&m); err != nil {
			return nil, err
		}
		if m.Error == "" {
			return nil, fmt.Errorf("%s", m.Message)
		}
		return nil, fmt.Errorf("%s", m.Error)
	}
	return resp, nil
}
