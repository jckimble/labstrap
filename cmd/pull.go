// Copyright © 2018 James Kimble
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//     http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package cmd

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// pullCmd represents the pull command
var pullCmd = &cobra.Command{
	Use:   "pull",
	Short: "Pulls Gitlab Snippets into directory",
	PreRunE: func(cmd *cobra.Command, args []string) error {
		if len(args) == 0 {
			return fmt.Errorf("Must Supply Atleast One Snippet Title")
		}
		return nil
	},
	RunE: func(cmd *cobra.Command, args []string) error {
		base := ""
		if cmd.Flags().Lookup("basedir") != nil {
			base = cmd.Flags().Lookup("basedir").Value.String()
		}
		for _, arg := range args {
			s, err := findSnippet(arg)
			if err != nil {
				return err
			}
			if err := parseSnippet(base, s); err != nil {
				return err
			}
		}
		return nil
	},
}

func init() {
	rootCmd.AddCommand(pullCmd)

	pullCmd.Flags().StringP("basedir", "b", ".", "base directory to run in")
	pullCmd.Flags().BoolP("overwrite", "o", false, "overwrite files")
	viper.BindPFlag("overwrite", pullCmd.Flags().Lookup("overwrite"))
}

func findSnippet(name string) (snippet, error) {
	resp, err := submitRequest("GET", fmt.Sprintf("%s/snippets", viper.GetString("gitlab")), nil)
	if err != nil {
		return snippet{}, err
	}
	defer resp.Body.Close()
	m := []snippet{}
	dec := json.NewDecoder(resp.Body)
	if err := dec.Decode(&m); err != nil {
		return snippet{}, err
	}
	for _, s := range m {
		if s.Title == name {
			return s, nil
		}
	}
	return snippet{}, fmt.Errorf("No Snippet Named %s", name)
}

func parseSnippet(base string, s snippet) error {
	if s.FileName == "labstrap.manifest" {
		if !viper.GetBool("quiet") {
			log.Printf("Downloading Manifest: %s", s.Title)
		}
		return parseManifest(base, s.RawUrl)
	} else {
		filename := s.FileName
		if base != "" {
			filename = filepath.Join(base, filename)
		}
		if !viper.GetBool("quiet") {
			log.Printf("Downloading File: %s", filename)
		}
		return downloadFile(filename, s.RawUrl)
	}
}

func downloadFile(file, url string) error {
	resp, err := submitRequest("GET", url, nil)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	if dir := filepath.Dir(file); dir != "" {
		os.MkdirAll(dir, 0755)
	}
	if !viper.GetBool("overwrite") {
		if _, err := os.Stat(file); err == nil {
			return fmt.Errorf("File Exists and overwrite is not set %s", file)
		}
	}
	out, err := os.Create(file)
	if err != nil {
		return err
	}
	defer out.Close()
	if _, err := io.Copy(out, resp.Body); err != nil {
		return err
	}
	return nil
}
func parseManifest(base string, url string) error {
	resp, err := submitRequest("GET", url, nil)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	m := manifest{}
	dec := json.NewDecoder(resp.Body)
	if err := dec.Decode(&m); err != nil {
		return err
	}
	for _, id := range m.Snippets {
		if base != "" && m.BaseDir != "" {
			base = filepath.Join(base, m.BaseDir)
		} else if base == "" && m.BaseDir != "" {
			base = m.BaseDir
		}
		if err := getSnippet(base, id.Id, id.Title); err != nil {
			return err
		}
	}
	return nil
}

func getSnippet(base string, id uint64, title string) error {
	resp, err := submitRequest("GET", fmt.Sprintf("%s/snippets/%d", viper.GetString("gitlab"), id), nil)
	if err != nil {
		return err
	}
	defer resp.Body.Close()
	s := snippet{}
	dec := json.NewDecoder(resp.Body)
	if err := dec.Decode(&s); err != nil {
		return err
	}
	if title != "" && title != s.Title {
		return fmt.Errorf("Title doesn't match snippet")
	}
	return parseSnippet(base, s)
}

type snippet struct {
	Id          uint64 `json:"id"`
	Title       string `json:"title"`
	FileName    string `json:"file_name"`
	Description string `json:"description"`
	RawUrl      string `json:"raw_url"`
	Visibility  string `json:"visibility"`
}

type manifest struct {
	BaseDir  string `json:"basedir"`
	Snippets []struct {
		Id    uint64 `json:"id"`
		Title string `json:"title"`
	} `json:"snippets"`
}
